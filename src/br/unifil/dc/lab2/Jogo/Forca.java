package Jogo;

import java.util.Random;
import java.util.Scanner;

public class Forca {
    public void singlePlayer() {
        Scanner teclado = new Scanner(System.in);

        String[] palavra = {"papagaio","pneumoultramicroscopicossilicovulcanoconiotico", "rinoceronte", "cachorro", "tamandua",
                "lince","telescopio", "computador", "relogio", "caderno", "dicionario","dentista", "professor", "mecanico",
                "engenheiro", "medico"};

        int index = new Random().nextInt(15);
        String palavraSelecionada = palavra[index];

        char[] traco = new char[palavraSelecionada.length()];
        for (int i = 0; i < palavraSelecionada.length(); i++) {
            traco[i] = '_';
        }

        int palavraString = palavra[index].length();
        int tentativas = 7;

        while (palavraString > 0 && tentativas > 0) {
            System.out.println(" ");
            for (int i = 0; i < palavraSelecionada.length(); i++) {
                System.out.print(" " + traco[i] + " ");
            }
            System.out.println(" ");

            System.out.println("Você tem " + tentativas + " chances de adivinhar");
            System.out.println("Digite uma letra: ");
            
            char letras = teclado.next().charAt(0);
            boolean iscorrect = false;
            for (int i = 0; i < traco.length; i++) {

                if (palavra[index].charAt(i) == letras) {
                    traco[i] = letras;
                    palavraString--;
                    iscorrect = true;
                }
            }

            if (!iscorrect) {
                tentativas--;
            }
        }

        if (palavraString == 0) {
            System.out.println("Parabéns!!!Você acertou!!!");
        } else {
            System.out.println("Infelizmente Você Perdeu");
            System.out.println("A palavra era " + palavraSelecionada);
        }
    }
}
