package Jogo;
import java.util.*;

public class MultiplayerForca {
    public void forcaMultijogador() {
        int pontos = 0;
        Scanner teclado = new Scanner(System.in);

        String[] palavra = {"papagaio", "rinoceronte", "cachorro", "tamandua", "lince","telescopio", "computador",
                "relogio", "caderno", "dicionario","dentista", "professor", "celular", "dromedalho", "mecanico",
                "engenheiro", "medico", "raquete"};

        int index = new Random().nextInt(17);
        String palavraSelecionada = palavra[index];

        char[] traco = new char[palavraSelecionada.length()];
        for (int i = 0; i < palavraSelecionada.length(); i++) {
            traco[i] = '_';
        }

        int palavraString = palavra[index].length();
        int tentativas = 7;

        while (palavraString > 0 && tentativas > 0) {
            System.out.println(" ");
            for (int i = 0; i < palavraSelecionada.length(); i++) {
                System.out.print(" " + traco[i] + " ");
            }
            System.out.println(" ");

            System.out.println("Você tem " + tentativas + " chances de adivinhar");
            System.out.println("Score: " + pontos);
            System.out.println("Digite uma letra: ");
            
            char letras = teclado.next().charAt(0);
            boolean iscorrect = false;
            for (int i = 0; i < traco.length; i++) {

                if (palavra[index].charAt(i) == letras) {
                    traco[i] = letras;
                    palavraString--;
                    iscorrect = true;
                    pontos++;
                }
            }

            if (!iscorrect) {
                tentativas--;
                pontos -= 3;
            }
        }

        if (palavraString == 0) {
            pontos += 5;
            System.out.println("Parabéns!!!Você acertou!!!");
            System.out.println("Sua pontoaçao: " + pontos);
        } else {
            System.out.println("Infelizmente Você Perdeu");
            System.out.println("A palavra era " + palavraSelecionada);
            System.out.println("Sua pontoaçao: " + pontos);
        }
    }    
}
