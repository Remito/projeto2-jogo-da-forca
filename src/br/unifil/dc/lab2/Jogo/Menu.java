package Jogo;

import java.io.IOException;
import java.util.*;

public class Menu {
    public static void main(String[] args) throws IOException {
        ConfessionServer mpf = new ConfessionServer();
        Forca spf = new Forca();
        Scanner scan = new Scanner(System.in);
        int opcao = 0;
        while(opcao != 9){
            try {
                System.out.println("Precione: 1 - 1 jogador; 2 - multijogadores; 9 - Sair");
                opcao = scan.nextInt();

                switch(opcao){
                    case 1:
                        spf.singlePlayer();
                    break;

                    case 2:
                        mpf.servidor();
                    break;
                    case 9:
                        System.out.println("Saindo....");
                    break;

                    default:
                        System.out.println("Opção inválida, selecione 1 para 1 jogador, 2 para multijogadores ou 9 para Sair");
                    break;
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
    }
}