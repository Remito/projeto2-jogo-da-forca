package Jogo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ConfessionServer {
    public void servidor() throws IOException {
        ServerSocket ss = new ServerSocket(27453);
        System.out.println("Aguarda conexão");

        Socket s = ss.accept();
        System.out.println("conexao aceita");
        System.out.println("Um novo cliente esta conectado: " + s);
        
        PrintWriter escritor = new PrintWriter(s.getOutputStream());

        escritor.println("Pressione a tecla enter para começar");
        escritor.flush();

        BufferedReader todoOuvidos = new BufferedReader(new InputStreamReader(s.getInputStream()));

        //StringBuilder tracejado;
        String msg ="";
        int palavraString = 0;
        int pontos = 0;
        String[] palavra = {"papagaio", "rinoceronte", "pneumoultramicroscopicossilicovulcanoconiotico",
                "tamandua", "lince","telescopio", "computador", "relogio", "caderno", "dicionario","dentista",
                "professor", "celular", "dromedalho", "mecanico", "engenheiro", "medico", "armario", "escada",
                "cachorro"};

        while (!msg.equals("Bye")){
            int index = new Random().nextInt(19);
            String palavraSelecionada = palavra[index];

            char[] traco = new char[palavraSelecionada.length()];
            for (int i = 0; i < palavraSelecionada.length(); i++) {
                traco[i] = '_';
            }

            palavraString = palavra[index].length();
            int tentativas = 7;

            msg = todoOuvidos.readLine();

            while (palavraString > 0 && tentativas > 0){

                for (int i = 0; i < palavraSelecionada.length(); i++) {
                    System.out.print(" " + traco[i] + " ");
                    escritor.print(" " + traco[i] + " " );
                }

                escritor.println(" || Tentativas: " + tentativas + " || Score: " + pontos + " || Digite uma letra: ");

                escritor.flush();

                msg = todoOuvidos.readLine();

                char letras = msg.charAt(0);
                boolean isCorrect = false;
                for (int i = 0; i < traco.length; i++) {

                    if (palavra[index].charAt(i) == letras) {
                        traco[i] = letras;
                        palavraString--;
                        isCorrect = true;
                        pontos++;
                    }
                }

                if (!isCorrect && !msg.equals("Bye")) {
                    tentativas--;
                    pontos -= 3;
                }

                if (msg.equals("Bye")){
                    s.close();
                }
            }
            if (palavraString == 0) {
                pontos += 5;
                escritor.println("Parabéns!!! Você acertou a palavra "+ palavraSelecionada +". Sua pontuação final é " + pontos + " Pressione a tecla enter para Recomeçar");
                escritor.flush();
            }else if(msg.equals("Bye")){
                escritor.println("Tchau :(... A palavra era " + palavraSelecionada + ", e a sua pontuação final é " + pontos);
                escritor.flush();
            }else{
                escritor.println("Infelizmente você errou. A palavra era " + palavraSelecionada + ", e sua pontuação final é " + pontos);
                escritor.flush();
            }
        }

        escritor.println("Fim");
    }
}