package Jogo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class ConfessionClient {
    public static void main(String[] args) {
        try {
            InetAddress ip = InetAddress.getByName("localhost");

            Socket confessionSocket = new Socket(ip,27453);

            PrintWriter out = new PrintWriter(confessionSocket.getOutputStream(),true);

            BufferedReader in = new BufferedReader(new InputStreamReader(confessionSocket.getInputStream()));
            
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

            String fromServer;
            String fromUser;
            
            while ((fromServer = in.readLine()) != null){
                System.out.println( fromServer);
                   if (fromServer.equals("Bye")){
                       break;
                   }
                fromUser = stdIn.readLine();

                if (fromUser != null){
                    //System.out.println("Letra: " + fromUser);
                    out.println(fromUser);
                }
            }
            confessionSocket.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}