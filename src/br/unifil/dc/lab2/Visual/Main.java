package Visual;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main extends JFrame {
    
    private Tela tela;
    private JButton enviar;
    private Forca game;    
    Graphics2D g2d;
    JTextField caixaTexto = new JTextField(15);

    public Main() {
        game.arrumaJogo();
        // Cria e configura botão Desenhar Arranjo
        enviar = new JButton("Enviar");
        enviar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String input = caixaTexto.getText();
                int tentativa = game.singlePlayer(input);  
                String desenho = "desenhoLivre";
                tela.setDesenho(desenho);
                tela.repaint();
                requestFocusInWindow();
            }
        });

        // Campo para abrigar e organizar os botões e campos de entrada
        JPanel inserirLetra = new JPanel(new FlowLayout());
        inserirLetra.add(enviar);
        inserirLetra.add(caixaTexto);
        caixaTexto.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String input = caixaTexto.getText();
                System.out.println(input);
                int tentativa = game.singlePlayer(input);
                Desenhos.forca(g2d, tentativa);
                String desenho = "desenhoLivre";
                tela.setDesenho(desenho);
                tela.repaint();
                requestFocusInWindow();
            }
        });
        // Cria e configura a tela do desenhista
        tela = new Tela(/*txtEntradaArranjo.getText()*/);
        tela.setPreferredSize(new Dimension(820, 600));

        // Container que organiza o posicionamento dos botões e da tela
        Container organizacao = getContentPane();
        organizacao.setLayout(new BorderLayout());
        organizacao.add(tela, BorderLayout.CENTER);
        organizacao.add(inserirLetra, BorderLayout.SOUTH);

        // Configurações de comportamento do aplicativo
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Jogo da Forca");
        pack();
        setVisible(true);
        requestFocus();
    }


    public static void main(String[] args) {
        // Código que inicializa o aplicativo gráfico
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        });
    }
}
