package Visual;

import java.util.Random;
import java.util.Scanner;

public class Forca {
    Scanner teclado = new Scanner(System.in);
    static String[] palavra = { "papagaio", "rinoceronte", "cachorro", "tamandua", "lince", "telescopio", "computador",
            "relogio", "caderno", "dicionario", "dentista", "professor", "mecanico", "engenheiro", "medico" };
    public static char[] tracadinho = {};
    static int index = new Random().nextInt(14);
    public static String selectedWord = palavra[index];
    static int palavraString = palavra[index].length();
    public static int tentativas = 7;

    public static void arrumaJogo() {
        char[] traco = new char[selectedWord.length()];
        tracadinho = traco;
        for (int i = 0; i < selectedWord.length(); i++) {
            traco[i] = '_';
        }
    }

    public static int singlePlayer(String letra) {

        if (palavraString > 0 && tentativas > 0) {
            System.out.println(" ");
            for (int i = 0; i < selectedWord.length(); i++) {
                System.out.print(" " + tracadinho[i] + " ");
            }
            System.out.println(" ");

            System.out.println("Você tem " + tentativas + " chances de adivinhar");
            System.out.println("Digite uma letra: ");
            
            char letras = letra.charAt(0);
            boolean iscorrect = false;
            for (int i = 0; i < tracadinho.length; i++) {

                if (palavra[index].charAt(i) == letras) {
                    tracadinho[i] = letras;
                    palavraString--;
                    iscorrect = true;
                }
            }

            if (!iscorrect) {
                tentativas--;
            }
        }else{
            if (palavraString == 0) {
                System.out.println("Parabéns!!!Você acertou!!!");
            } else {
                System.out.println("Infelizmente Você Perdeu");
                System.out.println("A palavra era " + selectedWord);
            }
        }
        return tentativas;
    }
}