package Visual;
import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Desenhos {
    static Graphics2D pincel;
    
    public static void forca(Graphics2D pincel, int chances) {
        switch(chances){
            case 7:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191, 50, 191, 100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50, 50, 50, 400);
                pincel.drawLine(50, 50, 190, 50);
            break;
            case 6:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191, 50, 191, 100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50, 50, 50, 400);
                pincel.drawLine(50, 50, 190, 50);
                // boneco
                pincel.fillOval(166, 100, 50, 50);
            break;
            case 5:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191, 50, 191, 100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50, 50, 50, 400);
                pincel.drawLine(50, 50, 190, 50);
                // boneco
                pincel.fillOval(166, 100, 50, 50);
                pincel.drawLine(191, 140, 191, 220);
            break;
            case 4:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191, 50, 191, 100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50, 50, 50, 400);
                pincel.drawLine(50, 50, 190, 50);
                // boneco
                pincel.fillOval(166, 100, 50, 50);
                pincel.drawLine(191, 140, 191, 220);
                pincel.drawLine(161, 270, 191, 220);
            break;
            case 3:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191, 50, 191, 100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50, 50, 50, 400);
                pincel.drawLine(50, 50, 190, 50);
                // boneco
                pincel.fillOval(166, 100, 50, 50);
                pincel.drawLine(191, 140, 191, 220);
                pincel.drawLine(161, 270, 191, 220);
                pincel.drawLine(221, 270, 191, 220);
            break;
            case 2:
                // poste
                pincel.setStroke(new BasicStroke(7));
                pincel.drawLine(191,50,191,100);
                pincel.setStroke(new BasicStroke(10));
                pincel.drawLine(50,50,50,400);        
                pincel.drawLine(50,50,190,50);
                // boneco
                pincel.fillOval(166, 100, 50, 50);
                pincel.drawLine(191,140,191,220);        
                pincel.drawLine(161,270,191,220);
                pincel.drawLine(221,270,191,220);
                pincel.drawLine(191,160,161,220);
            break;
            case 1:
            // poste
            pincel.setStroke(new BasicStroke(7));
            pincel.drawLine(191,50,191,100);
            pincel.setStroke(new BasicStroke(10));
            pincel.drawLine(50,50,50,400);        
            pincel.drawLine(50,50,190,50);
            // boneco
            pincel.fillOval(166, 100, 50, 50);
            pincel.drawLine(191,140,191,220);        
            pincel.drawLine(161,270,191,220);
            pincel.drawLine(221,270,191,220);
            pincel.drawLine(191,160,161,220);
            pincel.drawLine(191,160,221,220);
            break;
        }

        // Letras e traços
        pincel.setFont(new Font("Arial", Font.BOLD, 27));
        //pincel.drawString(palavra, 300, 300);
        char[] traco = new char[palavra.length()];
        for (int i = 0; i < palavra.length(); i++) {
            traco[i] = '_';
        }

        for (int i = 0; i < palavra.length(); i++) {
            pincel.drawString(" " + traco[i] + " ", 300 + i*30, 300);
        }
        pincel.drawString("Você tem " + chances + " chances de adivinhar",300,350);
        pincel.drawString("Digite uma letra: ", 300, 375);
    }

    static String palavra = Forca.selectedWord;
    static int chances = Forca.tentativas;
}