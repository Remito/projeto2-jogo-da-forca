package Visual;
import javax.swing.*;
import java.awt.*;

class Tela extends JPanel {
    
    public Tela() {
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.WHITE);
    }
    
    public Tela(String desenho) {
        this();
        this.desenhoEscolhido = desenho;
    }
    
    public void setDesenho(String desenho) {
        this.desenhoEscolhido = desenho;
    }
        
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        //game.singlePlayer(letra);
        Desenhos.forca(g2d, 7);
    }
    private String desenhoEscolhido = "";
    Forca game = new Forca();
}